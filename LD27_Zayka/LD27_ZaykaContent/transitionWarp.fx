sampler inputSampler : register(s0); // main

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoords  : TEXCOORD0;
};

float time;
float speed = 1;
bool warpOut;

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{

	float2 center = float2(0.5,0.5);
	float2 dir = input.TexCoords-center;
	
	if (warpOut) 
	{
		//float l = length(dir);
		//dir=-dir;
		float time2 = time-1;
	 	dir = normalize(dir);
		float2 newCoord = input.TexCoords+dir*(1.0-time2);
		float4 outColor = tex2D(inputSampler,newCoord);
		if (length(center-newCoord)>0.5) outColor=float4(0,0,0,1);	
		return outColor;
	}
	 else 
	{
		dir = normalize(dir);
		float s = speed*time;
		float2 newCoord = input.TexCoords+dir*s;
	
		float4 outColor = tex2D(inputSampler,newCoord);
		if (length(center-newCoord)>0.6)
			outColor=float4(0,0,0,1);	
		return outColor;
	}
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
