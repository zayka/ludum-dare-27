sampler inputSampler : register(s0); // main

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoords  : TEXCOORD0;
};


float sinus;
float cosinus;

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float x = input.TexCoords.x;
	float y = input.TexCoords.y;

	float p1 = (sin(x*32)+1)/2;
	float p2 = (sin(y*16)+1)/2;
	float p3 = (sin((x+y)*32)+1)/2;
	float newX = (p1+p2+p3+sinus);
	float newY = (p1+p2+p3+cosinus);
    float4 newcolor=tex2D(inputSampler,float2(x+0.05*newX,y+0.05*newY)-0.1);
    return newcolor;
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
