﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace LD27_Zayka
{
    public static class TextureManager
    {
        public static Dictionary<string, Texture2D> Textures = new Dictionary<string, Texture2D>();

        public static void Init()
        {
            ContentManager content = Cnt.game.Content;
            Load("Player", "Player");
            Load("PurpleBlock", "twily");
            Load("PinkieBlock", "pinkie");
            Load("RainbowStrike", "RainbowStrike");
            Load("Button", "button");
            Load("ButtonPlus", "buttonPlus");
            Load("ButtonMinus", "buttonMinus");
            Load("BGLevelUP","bg_levelupscreen");
            Load("abilityBG", "abilityBG");
            Load("Coolness", "abilities\\ab_coolness");
            Load("Rstrike", "abilities\\ab_rstrike");
            Load("Slowtime", "abilities\\ab_slowtime");
            Load("Speedstrut", "abilities\\ab_speedstrut");
            Load("Square", "abilities\\ab_square");
            Load("Dot", "blackDot");
            Load("WinScreen", "WinScreenBG");
            Load("StartScreen", "StartScreenBG");
            
            Load("LevelEmpty", "abilities\\powerLevel_empty");
            Load("LevelFill", "abilities\\powerLevel_fill");
            Load("RainblockStrike_action", "rainBlockStrike");
            Load("SpeedStrut_action", "speedStrut");
            

            Load("Cloud0", "cloud0");
            Load("Cloud1", "cloud1");
            Load("Cloud2", "cloud2");
            Load("Background", "bg_main");
        }

        
        static void Load(string name, string path)
        {
            ContentManager content = Cnt.game.Content;
            Texture2D tex = null;
            try
            {
                tex = content.Load<Texture2D>(path);
            }
            catch
            {
            }
            if (tex != null) Textures.Add(name, tex);
        }
    }
}
