using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD27_Zayka
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        public SpriteFont font;
        public static int screenWidth = 1024;
        public static int screenHeight = 768;
        public Camera camera;
        public Camera Camera { get { return camera; } }
        GraphicsDevice device;
        public static Random rnd = new Random();

        // Player rainbow;
        public ParticleEngine pEngine;
        public ScreenManager sManager;
        public Transition transition;

       

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            IsMouseVisible = true;
            //IsFixedTimeStep = false;
            //graphics.SynchronizeWithVerticalRetrace = false;
            Window.Title = "RainbowBlock!";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            pEngine = new ParticleEngine();
            device = graphics.GraphicsDevice;
            sManager = new ScreenManager(this);

            Cnt.game = this;
            Cnt.pEngine = pEngine;
            font = Content.Load<SpriteFont>("Font1");
            camera = new Camera();
            TextureManager.Init();

            Screen s = new MainScreen(sManager, "MainGame");
            sManager.AddScreen(s);
            s = new LevelUpScreen(sManager, "LevelUpScreen");
            sManager.AddScreen(s);
            s = new IntroScreen(sManager, "Intro");
            sManager.AddScreen(s);
            s = new FailScreen(sManager, "FailScreen");
            sManager.AddScreen(s);
            s = new WinOutro(sManager, "WinOutro");
            sManager.AddScreen(s);
            s = new WinScreen(sManager, "WinScreen");
            sManager.AddScreen(s);
            s = new StartScreen(sManager, "StartScreen");
            sManager.AddScreen(s);


            sManager.SetActiveScreen("StartScreen");
            transition = new Transition();

            Player p = new Player();
            Cnt.player = p;
#if DEBUG
            Components.Add(new FPSCounter.FPSCounter(this, font, spriteBatch));
#endif
        }

        protected override void UnloadContent() { }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            InputState.Update();
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (elapsed == 0) return;
            Cnt.elapsed = elapsed;
            pEngine.Update();
            if (transition.isInTransition) transition.Update();
            else
                sManager.Update(gameTime);

            //hud.Update();
            if (InputState.IsNewKeyPressed(Keys.Escape)) sManager.Pause();
#if DEBUG
            if (InputState.IsKeyPressed(Keys.D1)) { }
            if (InputState.IsNewKeyPressed(Keys.Escape)) this.Exit();
#endif
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (transition.isInTransition) transition.Draw(gameTime);
            else
                sManager.Draw(gameTime);
            pEngine.Draw(Camera.View);

#if DEBUG
            MainScreen ms = (MainScreen)sManager.GetScreenByName("MainGame");
            spriteBatch.Begin();
            spriteBatch.DrawString(font, "RS=" + Cnt.player.rainbowStikePower, new Vector2(10, 0 * 12), Color.LightGreen);
            spriteBatch.DrawString(font, "SS=" + Cnt.player.speedStrutPower, new Vector2(10, 1 * 12), Color.LightGreen);
            spriteBatch.DrawString(font, "ST=" + Cnt.player.slowTimePower, new Vector2(10, 2 * 12), Color.LightGreen);
            spriteBatch.DrawString(font, "C=" + Cnt.player.coolness, new Vector2(10, 3 * 12), Color.LightGreen);
            spriteBatch.End();
#endif
            base.Draw(gameTime);
        }
    }
}
