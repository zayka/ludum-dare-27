﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace LD27_Zayka
{
    [Serializable]
    public class Particle
    {
        public Texture2D texture;
        public Vector2 Origin;
        public Vector2 pos;
        public Vector2 velocity;
        public float angle;
        public float angleVelocity;
        public Vector4 color;
        public float alphaVelocity;
        public float size;
        public float sizeVelocity;
        float ttl;
        public bool isBlendStateAlpha;
        public float tMaxAlpha;

        float currentTime;
        Vector4 currentColor;
        private float omega;
        private static float i255 = 1f / 255;

        public bool Active;

        public bool toRemove;

        //public Rectangle pRectangle { get { return new Rectangle((int)pos.X, (int)pos.Y, texture.TextureResolution.width, texture.TextureResolution.height); } }

        public Particle()
        {
            Active = false;
            toRemove = false;
        
        }

        public void Update()
        {
            float elapsed = Cnt.elapsed; 

            if (currentTime < tMaxAlpha)
            {
                currentColor.W = MathHelper.Lerp(0.01f, color.W, currentTime / tMaxAlpha);
                currentTime += elapsed;
            }
            else
                currentColor.W -= elapsed * alphaVelocity;
            if (omega != 0)
            {
                Vector2 normal = new Vector2(velocity.Y, -velocity.X);
                normal.Normalize();
                normal *= omega;
                velocity += normal;
                omega -= Math.Sign(omega) * 1 * elapsed;
                if (Math.Abs(omega) < 1) omega = 0;
            }
                     
            ttl -= elapsed;
            pos += elapsed * velocity;
            angle += elapsed * angleVelocity;
            size = MathHelper.Clamp(size, size - elapsed * sizeVelocity, 0);
            if (ttl < 0 || size <= 0 || currentColor.W < 0) { toRemove = true; Active = false; }
           
        }


        float sinV(Vector2 v1, Vector2 v2)
        {
            float result = v1.X * v2.Y - v2.X * v1.Y;
            return result;
        }

        public void Draw()
        {
            
            Game1.spriteBatch.Draw(texture, pos, null, new Color(currentColor), angle, Origin, size, SpriteEffects.None, 1);
        }

        /// <summary>
        /// Установки частицы
        /// </summary>
        /// <param name="texture">Текстура</param>
        /// <param name="pos">Положение</param>
        /// <param name="speed">Скорость  точк/сек</param>
        /// <param name="angle">угол</param>
        /// <param name="angleVelocity">уголовая скорость рад/сек</param>
        /// <param name="color">цвет</param>
        /// <param name="alphaVelocity">скорость гашения цвета ед в сек.</param>
        /// <param name="size">размер</param>
        /// <param name="sizeVelocity">скорость уменьшения</param>
        /// <param name="ttl">время жизни в сек.</param>
        /// <param name="alphablend">альфабленд</param>
        /// <param name="layer">true = передний план</param>
        /// <param name="isInteractive">true = игрок воздействует на эту частицу</param>
        /// <param name="Mass">условная масса частицы</param>
        /// <param name="isSolid">true - ударяется о барьеры</param>
        public void SetParam(Texture2D texture, Vector2 pos, Vector2 speed,
                        float angle, float angleVelocity,
                        Vector4 color, float alphaVelocity,
                        float size, float sizeVelocity,
                        float ttl, bool alphablend, float tMaxAlpha)
        {
            this.texture = texture;
            Origin = new Vector2(texture.Width / 2, texture.Height / 2);

            this.pos = pos;
            this.velocity = speed;

            this.angle = angle;
            this.angleVelocity = angleVelocity;
            this.color = color;
            this.color.W *= i255;
            this.color.X *= i255;
            this.color.Y *= i255;
            this.color.Z *= i255;
            this.alphaVelocity = alphaVelocity * i255;
            this.size = size;
            this.sizeVelocity = sizeVelocity;
            this.ttl = ttl;
            this.toRemove = false;
            this.Active = true;
            this.isBlendStateAlpha = alphablend;
            this.currentColor = this.color;
            this.tMaxAlpha = tMaxAlpha;
            
            if (tMaxAlpha != 0)
            {
                this.currentColor.W = 0;
            }
            currentTime = 0;
            this.omega = 0;            
        }

        public override string ToString()
        {
            return Active.ToString();
        }
    }
}
