﻿using System;
using Microsoft.Xna.Framework;
using System.Xml;
using System.Reflection;
using Microsoft.Xna.Framework.Graphics;

namespace LD27_Zayka
{
    [Serializable]
    public class BaseEmitter
    {

        public Vector2 Pos { get { return position; } set { position = value; } }
        public Vector2 position;
        public Vector2 prevPos;
        public Texture2D texture;

        // настройки
        public float Pps { get { return 1f / (float)pTime; } set { pTime = 1 / value; } } // particles per second
        public float minSpeed = 40.0f;
        public float maxSpeed = 60.0f;
        public float posVar = 0.0f;
        public float alphaVel = 400;
        public float minSize = 1.0f;
        public float maxSize = 3.0f;
        public float sizeVel = 0.2f;
        public float ttl = 50;
        public Color color;
        public Color Color { get { return color; } set { color = value; } }
        public bool alphablend = false;
        public float angleMain = 0;
        public float angleSecondary = 0;
        //public float width = 500;
        //public float height = 1;
        public float startAngle = 0;
        public float angleVelocity = 1;
        public float tMaxAlpha = 0.1f;
        public bool enable;

        protected double pTime = 1 / 1000f; // 1/pps
        private double toMachTime = 0;
        private double totalElapsed = 0;

        public Vector2 emiterVelosity = Vector2.Zero;
        public double emitterTtl = 0;
        public bool isTemporalEmitter = false;

        protected static Random rnd = new Random(111222);
        protected delegate void GenerateParticle();
        protected GenerateParticle generate;
        protected Vector2 generatedDir;
        protected Vector2 generatedPos;
        protected ParticleEngine engine;

        public BaseEmitter(string folder, ParticleEngine engine)
        {
            texture = Cnt.game.Content.Load<Texture2D>(folder);
            enable = true;
            generate = Gen1;
            this.engine = engine;
        }

        protected void Gen1()
        {

            float angle = (float)rnd.NextDouble() * MathHelper.TwoPi;
            float x = (float)Math.Cos(angle);
            float y = (float)Math.Sin(angle);
            generatedDir = new Vector2(x, y);
            float pSpeed = RandomMinMax(minSpeed, maxSpeed);
            //Vector2 randomDir = new Vector2((float)rnd.NextDouble() * 1.0f - 0.5f, (float)rnd.NextDouble() * 1.0f - 0.5f);
            generatedPos = new Vector2((float)rnd.NextDouble() * 1.0f - 0.5f, (float)rnd.NextDouble() * 1.0f - 0.5f);
            generatedPos *= posVar;
            generatedDir *= pSpeed;
        
        }

        public void UpdateEmitter()
        {
            if (!enable) return;// расходимся, тут нечего излучать.
            double elapsed = Cnt.elapsed;
            //ParticleEngine engine = Cnt.pEngine;
            if (isTemporalEmitter)
            {
                emitterTtl -= elapsed;
                Pos += (float)elapsed * emiterVelosity;
            }

            double curTime = toMachTime;

            toMachTime += elapsed;
            totalElapsed += elapsed;
            double invElapsed = 0;
            if (toMachTime > pTime) invElapsed = 1 / totalElapsed;
            while (toMachTime > pTime)
            {
                Vector2 curPos = Vector2.Lerp(Pos, prevPos, (float)(toMachTime * invElapsed));
                if (generate != null)
                    generate();

                float pSize = RandomMinMax(minSize, maxSize);
                float randomAngleVelocity = RandomMinMax(angleVelocity * 0.9f, angleVelocity * 1.1f);
                
                engine.Add(texture, generatedPos + curPos, generatedDir,
                           startAngle,
                           randomAngleVelocity,
                           new Vector4(color.R, color.G, color.B, color.A),
                           alphaVel,
                           pSize,
                           sizeVel,
                           ttl,
                           alphablend,
                           tMaxAlpha
                           );
                
                toMachTime -= pTime;
                totalElapsed = 0;
            }
            prevPos = Pos;
        }

        public virtual void DrawEmitter()
        {
        }

        protected float RandomMinMax(float min, float max)
        {
            return (float)rnd.NextDouble() * (max - min) + min;
        }

        protected float Normal(float min, float max)
        {
            float sigma = (max - min) * 0.16667f;//   /6
            float tmp = 0;
            for (int i = 0; i < 12; i++)
            {
                tmp += (float)rnd.NextDouble();
            }
            tmp -= 6;
            tmp *= sigma;
            return tmp;
        }

        public override string ToString()
        {
            return position.ToString();
        }
    }
}
