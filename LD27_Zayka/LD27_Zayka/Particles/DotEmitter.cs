﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LD27_Zayka
{
    class DotEmitter:BaseEmitter
    {
        bool colorChange = false;
        public DotEmitter(string folder, ParticleEngine engine, bool colorChange=true)
            : base(folder, engine)
        {
            Color = Color.White;
            generate = GenerateDot;
            this.colorChange = colorChange;
        }

        public void GenerateDot()
        {
            float angle = (float)rnd.NextDouble() * MathHelper.TwoPi;
            float x = (float)Math.Cos(angle);
            float y = (float)Math.Sin(angle);
            generatedDir = new Vector2(x, y);

            generatedPos = new Vector2((float)rnd.NextDouble() * 1.0f - 0.5f, (float)rnd.NextDouble() * 1.0f - 0.5f);
            generatedDir *= RandomMinMax(minSpeed, maxSpeed);
            if (colorChange) Color = new Color((float)Game1.rnd.NextDouble(), (float)Game1.rnd.NextDouble(), (float)Game1.rnd.NextDouble());
            generatedPos *= posVar;
            angleVelocity = RandomMinMax(-angleMain, angleMain);
        }
    }
}
