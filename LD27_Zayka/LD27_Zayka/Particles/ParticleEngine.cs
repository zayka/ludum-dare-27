﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace LD27_Zayka
{
    public class ParticleEngine
    {
        //static int MAX_PARTICLES_ONSCREEN = 10000;
        //static Dictionary<string, GameTexture> explosionSprites;

        public int Count { get { return MainList.Length; } }
        int poolSize = 0;
        Particle[] MainList;
        Queue<Particle> freeParticles;
        //float currentTime;
        //float flushTime = 10;
       
        public ParticleEngine()
        {
            poolSize = 1000;
            MainList = new Particle[poolSize];
            freeParticles = new Queue<Particle>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                MainList[i] = new Particle();
                freeParticles.Enqueue(MainList[i]);
            }
        }

        public void Draw(Matrix View)
        {
            Game1.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, View);

            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active)
                {
                    MainList[i].Draw();
                }
            }

            Game1.spriteBatch.End();
          
        }

        public void Add(Texture2D texture, Vector2 pos, Vector2 speed,
                        float angle, float angleVelocity,
                        Vector4 color, float alphaVelocity,
                        float size, float sizeVelocity,
                        float ttl, bool alphablend, float tMaxAlpha)
        {
            Particle pollen = null;

            if (freeParticles.Count == 0) EnlargePool();
            pollen = freeParticles.Dequeue();
            pollen.SetParam(texture, pos, speed, angle, angleVelocity, color, alphaVelocity, size, sizeVelocity, ttl, alphablend, tMaxAlpha);

        }

        public void Update()
        {
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active)
                {
                    MainList[i].Update();
                    if (!MainList[i].Active) { freeParticles.Enqueue(MainList[i]); }
                }
            }
            /*
            currentTime += Cnt.elapsed;
            if (currentTime > flushTime) { Flush(); currentTime = 0; }
             */
        }

        void EnlargePool()
        {
            poolSize += 500;
            Particle[] newArray = new Particle[poolSize];
            MainList.CopyTo(newArray, 0);
            for (int i = poolSize - 500; i < newArray.Length; i++)
            {
                newArray[i] = new Particle();
                freeParticles.Enqueue(newArray[i]);
            }
            MainList = newArray;
        }


        private static float RandomMinMax(float min, float max)
        {
            return (float)Game1.rnd.NextDouble() * (max - min) + min;
        }
        
        public void Init()
        {
            poolSize = 1000;
            MainList = new Particle[poolSize];
            freeParticles = new Queue<Particle>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                MainList[i] = new Particle();
                freeParticles.Enqueue(MainList[i]);
            }
        }
        
        public void Flush()
        {
            int n = 0;
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active) n++;
            }
            Particle[] newArray = new Particle[n];
            n = 0;
            for (int i = 0; i < MainList.Length; i++)
            {
                if (MainList[i].Active) newArray[n++] = MainList[i];
            }
            MainList = newArray;
            poolSize = n;
        }        
    }
}
