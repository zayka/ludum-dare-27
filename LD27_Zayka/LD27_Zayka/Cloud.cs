﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace LD27_Zayka
{
    public class Cloud
    {
        static int N = 3;
        public Vector2 Position;
        Texture2D tex;
        Vector2 origin;
        float maxA = 20 * MathHelper.Pi / 180;
        float angle = 0;
        public float time = float.MaxValue;
        float omega = 20;
        Player player;
        Player Player { get { if (player == null) player = Cnt.player; return player; } }
        public float fi = 0;
        public bool isTremble = false;
        public float HP;
        public float maxHP = 100;

        public Rectangle rect { get { return new Rectangle((int)(Position.X - origin.X), (int)(Position.Y - origin.Y), tex.Width, tex.Height); } }

        public Cloud()
        {
            tex = TextureManager.Textures["Cloud" + Game1.rnd.Next(N)];
            Position = new Vector2((float)Game1.rnd.NextDouble() * Game1.screenWidth, (float)Game1.rnd.NextDouble() * Game1.screenHeight * 0.8f);
            origin = new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            //player = (Cnt.game.sManager.GetScreenByName("MainGame") as MainScreen).rainbow;
            HP = maxHP;
        }


        public void Update()
        {
            if (Player.rect.Intersects(rect))
            {
                if (!isTremble)
                {
                    time = 0;
                    fi = (float)(Game1.rnd.NextDouble() * MathHelper.PiOver2 - MathHelper.PiOver4);
                    isTremble = true;
                }

                //-HP +score
                HP -= Cnt.elapsed * player.coolness * player.DPS;
                HP = MathHelper.Clamp(HP, 0, maxHP);
                player.AddScore(Cnt.elapsed * player.coolness * player.DPS);
            }
            time += Cnt.elapsed;
            angle = (float)(maxA * Math.Pow(Math.E, -time * 0.7f) * Math.Sin(omega * time + fi));
            if (Math.Abs(angle) < 0.005f) isTremble = false;
            else { HP -= 0.5f * Math.Abs(angle); }

        }

        public void Draw()
        {
            Color c = Color.Lerp(new Color(0, 0, 0, 0.2f), Color.White, HP / maxHP);
            SpriteBatch sb = Game1.spriteBatch;
            sb.Draw(tex, Position, null, c, angle, origin, 1, SpriteEffects.None, 1);
        }

        public void Draw(Vector2 pos)
        {
            Color c = Color.Lerp(Color.Transparent, Color.White, HP / maxHP);
            SpriteBatch sb = Game1.spriteBatch;
            sb.Draw(tex, pos, null, c, angle, origin, 1, SpriteEffects.None, 1);
        }
    }
}
