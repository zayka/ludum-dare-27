﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace LD27_Zayka
{
    class WinOutro:Screen
    {
        bool inProgress = false;
        bool isOutroEnd { get { return actionNum > allActions.Count; } }
        int actionNum = 0;
        public delegate void NPCAction(float param1, object param2, NPCCallBack callback);
        public delegate void NPCCallBack();
        NPC twily, rainbow;
        WinScreen nextScreen;
        WinScreen NextScreen { get { if (nextScreen == null) nextScreen = (WinScreen)Cnt.game.sManager.GetScreenByName("WinScreen"); return nextScreen; } }
        //List<Cloud> clouds;
        //List<float> speeds;
        //bool startCloud = false;
        //bool cloudsOnline = false;
        //float cloudTime = 0;
        //float cloudEndTime = 1.5f;

        List<ActionStruct> allActions;
        Button skipOutro;
        Texture2D bgTex;


        public WinOutro(ScreenManager manager, string name)
            : base(manager, name)
        {
            bgTex = TextureManager.Textures["Background"];
            skipOutro = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.85f), (int)(Game1.screenHeight * 0.9f), 90, 40), "SKIP");
            skipOutro.OnClick += SkipOutro;
            twily = new NPC(new Vector2(600, Cnt.ground), TextureManager.Textures["PurpleBlock"]);
            rainbow = new NPC(new Vector2(500, Cnt.ground - 5), TextureManager.Textures["Player"]);
            allActions = new List<ActionStruct>();

            ActionStruct act = new ActionStruct();
            act.callback = CommonCallBack;

            

            act.action = rainbow.TalkStart;
            act.parameter1 = 1.0f;
            act.parameter2 = "What'd I say?";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 0.7f;
            act.parameter2 = "TEN";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 0.7f;
            act.parameter2 = "SECONDS";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 1.0f;
            act.parameter2 = "FLAT!";
            allActions.Add(act);

            act.action = twily.TalkStart;
            act.parameter1 = 1.2f;
            act.parameter2 = "WOW!";
            allActions.Add(act);
            //

            act.action = rainbow.TalkStart;
            act.parameter1 = 2.0f;
            act.parameter2 = "I'd never leave Blockville hanging";
            allActions.Add(act);
        }


        public override void Update(float elapsed)
        {
         //   skipOutro.Update();
            if (isOutroEnd)
            {
             //  if (InputState.IsLeftButtonClick()) 
                Cnt.game.transition.Transit(NextScreen, 2);
                return;
            }
            if (actionNum > allActions.Count) { return; } 

            if (!inProgress)
            {

                inProgress = true;
                if (actionNum == allActions.Count) { actionNum++; return; } 
                ExecuteAction(allActions[actionNum]);
                actionNum++;
            }

            twily.Update();
            rainbow.Update();


            base.Update(elapsed);
        }

        public override void Draw(float elapsed)
        {
            SpriteBatch sb = Game1.spriteBatch;
            sb.Begin();
            sb.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);
      //      skipOutro.Draw();
            twily.Draw();
            rainbow.Draw();
            sb.End();
            base.Draw(elapsed);
        }

        public void CommonCallBack()
        {
            inProgress = false;
        }

        void ExecuteAction(ActionStruct a)
        {
            a.action(a.parameter1, a.parameter2, a.callback);
        }


        void SkipOutro(object sender, EventArgs e)
        {
            Cnt.game.transition.Transit(NextScreen, 1);
        }

    }
}
