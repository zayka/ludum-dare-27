﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace LD27_Zayka
{
    public class MainScreen : Screen
    {
        // public Player rainbow;
        Screen failScreen;
        Screen FailScreen
        {
            get
            {
                if (failScreen == null)
                {
                    failScreen = Cnt.game.sManager.GetScreenByName("FailScreen");
                }
                return failScreen;
            }
        }

        Screen winScreen;
        Screen WinScreen
        {
            get
            {
                if (winScreen == null)
                {
                    winScreen = Cnt.game.sManager.GetScreenByName("WinOutro");
                }
                return winScreen;
            }
        }
        List<Cloud> clouds;
        public List<Cloud> Clouds { get { return clouds; } set { clouds = value; } }
        public int maxCloud = 99;
        public float levelTime = 10;

        NPC twily;
        Texture2D bgTex;
        delegate void UseSkill(float elapsed);
        UseSkill currentSkill = null;
        UseSkill DrawSkill = null;

        Player rainbow { get { return Cnt.player; } }
        HUD hud;


        bool inAction = false;
        #region RainblockStrike
        public float RBTime = 999;
        Vector2 RBPos;
        float RBspeed = 2000;
        #endregion

        #region Speedstrut
        public float SSTime = 999;
        List<Vector2> SSstartPos = new List<Vector2>();
        List<Vector2> SSendPos = new List<Vector2>();
        float SSspeed = 50;
        #endregion

        #region SlowTime
        public float STTime = 999;
        public bool isSlowTimeActive = true;
        float STdelay = 3.0f;
        #endregion

        SoundEffectInstance whooshSound;

        public MainScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
            //rainbow = Cnt.player;//new Player();
            clouds = new List<Cloud>();

            for (int i = 0; i < maxCloud; i++)
            {
                Cloud c = new Cloud();
                clouds.Add(c);
            }


            twily = new NPC(new Vector2(600, Cnt.ground), TextureManager.Textures["PurpleBlock"]);
            twily.StandStart(11, null, null);
            bgTex = TextureManager.Textures["Background"];

            hud = new HUD(this);
            canPause = true;

            SoundEffect whooshEffect = Cnt.game.Content.Load<SoundEffect>("Sound\\whoosh");
            whooshSound = whooshEffect.CreateInstance();
        }

        public override void Update(float elapsed)
        {
            SongManager.Play("MainTheme");
            if (isSlowTimeActive)
            {
               // elapsed /= (Cnt.player.slowTimePower + 1);
                if (STTime > STdelay *Cnt.player.slowTimePower)
                    isSlowTimeActive = false;
                //levelTime -= elapsed / (Cnt.player.slowTimePower + 1);
            }
            else
                levelTime -= elapsed;
            if (levelTime < 0) Cnt.game.transition.Transit(FailScreen, 0);

            /*
            if (InputState.IsNewKeyPressed(Keys.F1))
                Cnt.game.transition.Transit(NextScreen, 0.5f);
            */
            if (Clouds.Count == 0) Cnt.game.transition.Transit(WinScreen, 0.0f);

            for (int i = 0; i < Clouds.Count; i++)
            {
                Cloud c = Clouds[i];               
                c.Update();
                if (c.HP <= 0)
                {
                    Clouds.RemoveAt(i--);
                    //
                    SoundEffect whooshEffect = Cnt.game.Content.Load<SoundEffect>("Sound\\111");
                    whooshEffect.CreateInstance().Play();
                    //
                  //  whooshSound.Play();
                }
            }


            RBTime += elapsed;
            SSTime += elapsed;
            STTime += elapsed;
            HandleInput();

            if (!inAction)
            {
                rainbow.Update();
            }
            else
            {
                if (currentSkill != null) currentSkill(elapsed);
                else inAction = false;
            }

            Vector2 dir = -rainbow.pos + twily.pos;
            twily.angle = (float)Math.Atan2(dir.Y, dir.X) - MathHelper.PiOver2;
            twily.Update();
            hud.Update();
            base.Update(elapsed);
        }

        private void HandleInput()
        {

            if (!inAction && InputState.IsNewKeyPressed(Keys.Q) && Cnt.player.isHaveRainbowStrike && RBTime > Cnt.player.rainbowStrikeCD)
            {
                inAction = true;
                RBPos = new Vector2(Game1.screenWidth * 0.5f, -200);
                currentSkill = UseRBStrike;
                RBTime = 0;
                DrawSkill = DrawRBStrike;
            }

            if (!inAction && InputState.IsNewKeyPressed(Keys.W) && Cnt.player.isHaveSpeedStrut && SSTime > Cnt.player.speedStrutCD)
            {
                inAction = true;
                int N = Game1.rnd.Next(4) + 2;
                SSstartPos.Clear();
                SSendPos.Clear();
                for (int i = 0; i < N; i++)
                {
                    SSstartPos.Add(new Vector2(Game1.screenWidth + 600, (float)Game1.rnd.NextDouble() * (Game1.screenHeight - 200)));
                    SSendPos.Add(new Vector2(-600, (float)Game1.rnd.NextDouble() * (Game1.screenHeight - 200)));
                }
                SSTime = 0;
                currentSkill = UseSStrut;
                DrawSkill = DrawSStrut;
            }

            if (InputState.IsNewKeyPressed(Keys.E) && Cnt.player.isHaveSlowTime && STTime > Cnt.player.slowTimeCD)
            {
                isSlowTimeActive = true;
                STTime = 0;
            }

        }

        public override void Draw(float elapsed)
        {
            Cnt.game.GraphicsDevice.Clear(Color.DarkBlue);
            SpriteBatch spriteBatch = Game1.spriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);


            if (currentSkill == null)
                rainbow.Draw();

            twily.Draw();
            foreach (var c in clouds)
            {
                c.Draw();
            }
            if (DrawSkill != null)
            {
                DrawSkill(elapsed);
            }
            hud.Draw();
            spriteBatch.End();

            base.Draw(elapsed);
        }

        public override void OnLine()
        {
            levelTime = 10;
            Cnt.player.pos = new Vector2(500, Cnt.ground);
            SSTime = RBTime = STTime = 999;
            SSstartPos.Clear();
            SSendPos.Clear();
            inAction = false;
            currentSkill = null;
            DrawSkill = null;
            SongManager.Play("MainTheme");
            base.OnLine();
        }


        void UseRBStrike(float elapsed)
        {
            RBPos.Y += elapsed * RBspeed;
            if (RBPos.Y > Game1.screenWidth + 200)
            {
                inAction = false;
                currentSkill = null;
            }
            Rectangle r = new Rectangle(0, (int)RBPos.Y - 150, 2000, 300);
            foreach (var c in clouds)
            {
                if (c.rect.Intersects(r))
                {
                    if (!c.isTremble)
                    {
                        c.time = 0;
                        c.fi = (float)(Game1.rnd.NextDouble() * MathHelper.PiOver2 - MathHelper.PiOver4);
                        c.isTremble = true;
                    }

                    //-HP +score
                    c.HP -= Cnt.elapsed * Cnt.player.rainbowStikePower * Cnt.player.DPS * 0.5f;
                    c.HP = MathHelper.Clamp(c.HP, 0, c.maxHP);
                    Cnt.player.AddScore(Cnt.elapsed * Cnt.player.rainbowStikePower);
                }
            }

        }
        void DrawRBStrike(float elapsed)
        {
            Texture2D tex = TextureManager.Textures["RainblockStrike_action"];
            Vector2 origin = new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            Game1.spriteBatch.Draw(tex, RBPos, null, Color.White, 0, origin, 1, SpriteEffects.None, 1);
        }


        void UseSStrut(float elapsed)
        {
            for (int i = 0; i < SSstartPos.Count; i++)
            {
                Vector2 dir = SSendPos[i] - SSstartPos[i];
                dir.Normalize();
                dir *= SSspeed;
                SSstartPos[i] += dir;
                StrutCollision(SSstartPos[i]);
                if (SSstartPos[i].X < -600)
                {
                    SSendPos.RemoveAt(i);
                    SSstartPos.RemoveAt(i--);
                }
            }
            if (SSstartPos.Count == 0)
            {
                inAction = false;
                currentSkill = null;
                DrawSkill = null;
            }
        }

        void DrawSStrut(float elapsed)
        {
            Texture2D tex = TextureManager.Textures["SpeedStrut_action"];
            Vector2 origin = new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            float angle = 0;
            for (int i = 0; i < SSstartPos.Count; i++)
            {
                Vector2 dir = SSendPos[i] - SSstartPos[i];
                angle = (float)Math.Atan2(dir.Y, dir.X);
                Game1.spriteBatch.Draw(tex, SSstartPos[i], null, Color.White, angle, origin, new Vector2(1.5f, 0.7f), SpriteEffects.None, 1);
            }
        }

        void StrutCollision(Vector2 position)
        {
            Rectangle r = new Rectangle((int)position.X - 500, (int)position.Y - 75, 2000, 150);
            foreach (var c in clouds)
            {
                if (c.rect.Intersects(r))
                {
                    if (!c.isTremble)
                    {
                        c.time = 0;
                        c.fi = (float)(Game1.rnd.NextDouble() * MathHelper.PiOver2 - MathHelper.PiOver4);
                        c.isTremble = true;
                    }

                    //-HP +score
                    c.HP -= Cnt.elapsed * Cnt.player.speedStrutPower * Cnt.player.DPS * 0.15f;
                    c.HP = MathHelper.Clamp(c.HP, 0, c.maxHP);
                    Cnt.player.AddScore(Cnt.elapsed * Cnt.player.speedStrutPower);
                }
            }
        }
    }
}
