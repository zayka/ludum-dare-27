﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LD27_Zayka
{
    class StartScreen:Screen
    {

         IntroScreen nextScreen;
         IntroScreen NextScreen { get { if (nextScreen == null) nextScreen = (IntroScreen)Cnt.game.sManager.GetScreenByName("Intro"); return nextScreen; } }
        

        
        Button startButton, exitButton;
        Texture2D bgTex;

        public StartScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
            startButton = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.43f), (int)(Game1.screenHeight * 0.8f), 150, 40), "START");
            startButton.OnClick += startClick;
            exitButton = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.43f), (int)(Game1.screenHeight * 0.9f), 150, 40), "EXIT");
            exitButton.OnClick += exitClick;
            bgTex = TextureManager.Textures["StartScreen"];
        }

        public override void Update(float elapsed)
        {
            startButton.Update();
            exitButton.Update();

            base.Update(elapsed);
        
        }

        public override void Draw(float elapsed)
        {
            SpriteBatch sb = Game1.spriteBatch;
            sb.Begin();
            sb.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);
            startButton.Draw();
            exitButton.Draw();
            sb.End();
            base.Draw(elapsed);
        }


        void startClick(object sender, EventArgs e)
        {
            Cnt.game.transition.Transit(NextScreen, 1);
        }

        void exitClick(object sender, EventArgs e)
        {
            Cnt.game.Exit();
        }

        public override void OnLine()
        {
            Cnt.player = new Player();
            base.OnLine();
        }

    }
}
