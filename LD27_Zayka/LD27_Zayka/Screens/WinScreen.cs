﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LD27_Zayka
{
    class WinScreen:Screen
    {
        StartScreen nextScreen;
        StartScreen NextScreen { get { if (nextScreen == null) nextScreen = (StartScreen)Cnt.game.sManager.GetScreenByName("StartScreen"); return nextScreen; } }
        

        
        Button continueB;
        Texture2D bgTex;

        public WinScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
            continueB = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.43f), (int)(Game1.screenHeight * 0.8f), 150, 40), "Continue");
            continueB.OnClick += ContinueB;
            bgTex = TextureManager.Textures["WinScreen"];
        }

        public override void Update(float elapsed)
        {
            continueB.Update();
          

            base.Update(elapsed);
        
        }

        public override void Draw(float elapsed)
        {
            SpriteBatch sb = Game1.spriteBatch;
            sb.Begin();
            sb.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);
            continueB.Draw();
          
            sb.End();
            base.Draw(elapsed);
        }


        void ContinueB(object sender, EventArgs e)
        {
            Cnt.game.transition.Transit(NextScreen, 1);
        }

    }

}
