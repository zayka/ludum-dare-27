﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace LD27_Zayka
{
    // диалог после фейла
    public class FailScreen : Screen
    {
        LevelUpScreen nextScreen;
        MainScreen gameScreen;
        List<ActionStruct> allActions;
        bool inProgress = false;
        NPC pinkie, twily, rainbow;

        bool isFailEnd { get { return actionNum > allActions.Count; } }
        int actionNum = 0;
        Texture2D bgTex;
        Button skipFail;


        SoundEffectInstance warpSound;

        public FailScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
            nextScreen = Cnt.game.sManager.GetScreenByName("LevelUpScreen") as LevelUpScreen;
            gameScreen = Cnt.game.sManager.GetScreenByName("MainGame") as MainScreen;
            allActions = new List<ActionStruct>();
            pinkie = new NPC(new Vector2(Game1.screenWidth + 100, Cnt.ground), TextureManager.Textures["PinkieBlock"]);
            twily = new NPC(new Vector2(600, Cnt.ground), TextureManager.Textures["PurpleBlock"]);
            rainbow = new NPC(new Vector2(500, Cnt.ground - 5), TextureManager.Textures["Player"]);
            bgTex = TextureManager.Textures["Background"];
            skipFail = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.85f), (int)(Game1.screenHeight * 0.9f), 90, 40), "SKIP");
            skipFail.OnClick += SkipFail;

            ActionStruct act = new ActionStruct();
            act.callback = CommonCallback;

            
            act.action = rainbow.TalkStart;
            act.parameter1 = 3;
            act.parameter2 = "I can't believe I failed....";
            act.callback = CommonCallback;
            allActions.Add(act);


            act.action = pinkie.WalkStart;
            act.parameter1 = 250;
            act.parameter2 = new Vector2(650, Cnt.ground);
            allActions.Add(act);

            act.action = pinkie.TalkStart;
            act.parameter1 = 2;
            act.parameter2 = "You always can travel back in time and try again!";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 2;
            act.parameter2 = "What are you talking about, PinkBlock?";
            allActions.Add(act);

            
            //pinkie.pos = new Vector2(650, Cnt.ground);
            act.action = pinkie.TalkStart;
            act.parameter1 = 2;
            act.parameter2 = "Timetravel, silly!!";
            allActions.Add(act);


            SoundEffect warpSoundEffect = Cnt.game.Content.Load<SoundEffect>("Sound\\warp");
            warpSound = warpSoundEffect.CreateInstance();

        }

        public override void Draw(float elapsed)
        {
            // gameScreen.Draw(elapsed);
            Cnt.game.GraphicsDevice.Clear(Color.DarkBlue);
            SpriteBatch sb = Game1.spriteBatch;
            sb.Begin();
            sb.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);
           
            foreach (var c in gameScreen.Clouds)
            {
                c.Draw();
            }

            twily.Draw();
            rainbow.Draw();
            pinkie.Draw();
            skipFail.Draw();
            sb.End();

            base.Draw(elapsed);
        }



        public override void Update(float elapsed)
        {
            skipFail.Update();
            if (isFailEnd)
            {
                //if (InputState.IsLeftButtonClick()) 
                    Cnt.game.transition.Transit(nextScreen,2, Transition.TType.Warp);
                    warpSound.Play();
                return;
            }

            if (InputState.IsNewKeyPressed(Keys.Space)) SkipFail(null, EventArgs.Empty);

            if (actionNum > allActions.Count) { return; } //end fail

            if (!inProgress)
            {

                inProgress = true;
                if (actionNum == allActions.Count) { actionNum++; return; } //end fail
                ExecuteAction(allActions[actionNum]);
                actionNum++;
            }

            twily.Update();
            rainbow.Update();
            pinkie.Update();
            base.Update(elapsed);
        }



        public void CommonCallback()
        {
            inProgress = false;
        }

        void ExecuteAction(ActionStruct a)
        {
            a.action(a.parameter1, a.parameter2, a.callback);
        }

        void SkipFail(object sender, EventArgs e)
        {
            Cnt.game.transition.Transit(nextScreen, 1);
        }

        public override void OnLine()
        {

            twily.Reset(new Vector2(600, Cnt.ground));
            pinkie.Reset(new Vector2(Game1.screenWidth + 100, Cnt.ground));
            rainbow.Reset(new Vector2(500, Cnt.ground - 5));
            actionNum = 0;
            inProgress = false;
            base.OnLine();
        }
    }
}
