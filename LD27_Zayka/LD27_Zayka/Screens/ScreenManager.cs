﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD27_Zayka
{
    public class ScreenManager
    {
        List<Screen> screens = new List<Screen>();
        Screen currentScreen;
        public bool inPause = false;
        SpriteFont font;

        public ScreenManager(Game1 game)
        {
            font = game.Content.Load<SpriteFont>("AbilityFont");
        }

        public void Update(GameTime gt)
        {

            if (currentScreen == null) return;
            if (!inPause)
            {
                currentScreen.Update((float)gt.ElapsedGameTime.TotalSeconds);
            }
        }

        public void Draw(GameTime gt)
        {
            if (currentScreen == null) return;
            currentScreen.Draw((float)gt.ElapsedGameTime.TotalSeconds);
            if (inPause)
            {
                Game1.spriteBatch.Begin();

                Game1.spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), new Color(0, 0, 0, 0.5f));
                string text = "PAUSE";
                Vector2 textOrigin = font.MeasureString(text) * 0.5f;
                Game1.spriteBatch.DrawString(font, text, new Vector2(Game1.screenWidth * 0.5f, Game1.screenHeight * 0.5f), Color.White, 0, textOrigin, 1, SpriteEffects.None, 1);
                Game1.spriteBatch.End();
            }
        }



        public void AddScreen(Screen scr)
        {
            screens.Add(scr);
        }

        public Screen GetScreenByName(string name)
        {
            Screen scr = screens.Find(s => s.screenName == name);
            if (scr != null) return scr;
            else
            { return null; }
            //    throw new Exception("Screen not found.");
        }

        public void SetActiveScreen(string name)
        {
            currentScreen = GetScreenByName(name);
            //      currentScreen.OnLine();
        }

        public void SetActiveScreen(Screen s)
        {
            currentScreen = s;
            //      s.OnLine();
        }


        public Screen GetActiveScreen()
        {
            return currentScreen;
        }

        public void Pause()
        {
            if (currentScreen.canPause)
                inPause ^= true;
        }
    }
}
