﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LD27_Zayka
{
    struct ActionStruct
    {
        public IntroScreen.NPCAction action;
        public float parameter1;
        public object parameter2;
        public IntroScreen.NPCCallBack callback;
    }

    public class IntroScreen : Screen
    {

        bool inProgress = false;
        bool isIntroEnd { get { return actionNum > allActions.Count; } }
        int actionNum = 0;
        public delegate void NPCAction(float param1, object param2, NPCCallBack callback);
        public delegate void NPCCallBack();
        NPC twily, rainbow;
        MainScreen nextScreen;
        List<Cloud> clouds;
        List<float> speeds;
        bool startCloud = false;
        bool cloudsOnline = false;
        float cloudTime = 0;
        float cloudEndTime = 1.5f;

        List<ActionStruct> allActions;
        Button skipIntro;
        Texture2D bgTex;


        public IntroScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
            twily = new NPC(new Vector2(150, Cnt.ground), TextureManager.Textures["PurpleBlock"]);
            rainbow = new NPC(new Vector2(600, Cnt.ground - 90), TextureManager.Textures["Player"]);
            nextScreen = Cnt.game.sManager.GetScreenByName("MainGame") as MainScreen;
            skipIntro = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth * 0.85f), (int)(Game1.screenHeight * 0.9f), 90, 40), "SKIP");
            skipIntro.OnClick += SkipIntro;
            bgTex = TextureManager.Textures["Background"];
            clouds = new List<Cloud>();
            speeds = new List<float>();
            for (int i = 0; i < nextScreen.maxCloud; i++)
            {
                Cloud c = new Cloud();
                clouds.Add(c);
                speeds.Add((float)Game1.rnd.NextDouble() * 1000 + 800);
            }

            allActions = new List<ActionStruct>();
            ActionStruct act = new ActionStruct();
            act.action = twily.WalkStart;
            act.parameter1 = 50;
            act.parameter2 = new Vector2(250, Cnt.ground);
            act.callback = CloudCallback;
            allActions.Add(act);


            act.action = twily.TalkStart;
            act.parameter1 = 1;
            act.parameter2 = "?";
            act.callback = CommonCallBack;
            allActions.Add(act);


            act.action = twily.TalkStart;
            act.parameter1 = cloudEndTime;
            act.parameter2 = "!!";
            act.callback = CommonCallBack;
            allActions.Add(act);



            act.action = twily.TalkStart;
            act.parameter1 = 3;
            act.parameter2 = "Where is those wheather blocks when they are needed.";
            allActions.Add(act);

            act.action = twily.TalkStart;
            act.parameter1 = 3;
            act.parameter2 = " Im sure RainbowBlock sleep as always";
            allActions.Add(act);


            act.action = twily.WalkStart;
            act.parameter1 = 150;
            act.parameter2 = new Vector2(600, Cnt.ground);
            act.callback = CommonCallBack;
            allActions.Add(act);

            act.action = twily.TalkStart;
            act.parameter1 = 1;
            act.parameter2 = "Hey you!";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 1;
            act.parameter2 = "What?";
            allActions.Add(act);


            act.action = twily.TalkStart;
            act.parameter1 = 2;
            act.parameter2 = "what about these clouds? You must keep sky clear.";
            allActions.Add(act);


            act.action = rainbow.WalkStart;
            act.parameter1 = 250;
            act.parameter2 = new Vector2(500, Cnt.ground);
            act.callback = CommonCallBack;
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 2;
            act.parameter2 = "I could clear this sky in ";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 0.7f;
            act.parameter2 = "TEN";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 0.7f;
            act.parameter2 = "SECONDS";
            allActions.Add(act);

            act.action = rainbow.TalkStart;
            act.parameter1 = 0.7f;
            act.parameter2 = "FLAT!";
            allActions.Add(act);


            act.action = twily.TalkStart;
            act.parameter1 = 2.0f;
            act.parameter2 = "Prove it";
            allActions.Add(act);



        }

        public override void Update(float elapsed)
        {
            skipIntro.Update();
            if (isIntroEnd)
            {
               // if (InputState.IsLeftButtonClick()) 
                    Cnt.game.transition.Transit(nextScreen, 0);
                return;
            }

            if (InputState.IsNewKeyPressed(Keys.Space)) SkipIntro(null, EventArgs.Empty);

            if (actionNum > allActions.Count) { return; } //end intro

            if (!inProgress)
            {

                inProgress = true;
                if (actionNum == allActions.Count) { actionNum++; return; } //end intro
                ExecuteAction(allActions[actionNum]);
                actionNum++;
            }

            twily.Update();
            rainbow.Update();

            if (startCloud)
            {
                cloudTime += elapsed;
                if (cloudTime > cloudEndTime) { startCloud = false; cloudsOnline = true; nextScreen.Clouds = clouds; }
            }

            base.Update(elapsed);
        }

        public override void Draw(float elapsed)
        {
            SpriteBatch sb = Game1.spriteBatch;
            sb.Begin();
            sb.Draw(bgTex, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), Color.White);
            skipIntro.Draw();
            if (startCloud)
            {
                Vector2 center = new Vector2(Game1.screenWidth * 0.5f, Game1.screenHeight * 0.5f);
                for (int i = 0; i < clouds.Count; i++)
                {
                    Vector2 dir = center - clouds[i].Position;
                    dir.Normalize();
                    float t = cloudTime / cloudEndTime;
                    Vector2 startPos = clouds[i].Position - dir * speeds[i];
                    Vector2 newPos = Vector2.Lerp(startPos, clouds[i].Position, t);
                    clouds[i].Draw(newPos);
                }
            }
            if (cloudsOnline)
            {
                foreach (var c in clouds)
                {
                    c.Draw();
                }
            }
            twily.Draw();
            rainbow.Draw();
            sb.End();
            base.Draw(elapsed);
        }

        public void CommonCallBack()
        {
            inProgress = false;
        }

        public void CloudCallback()
        {
            inProgress = false;
            startCloud = true;
        }

        void ExecuteAction(ActionStruct a)
        {
            a.action(a.parameter1, a.parameter2, a.callback);
        }

        void SkipIntro(object sender, EventArgs e)
        {
            Cnt.game.transition.Transit(nextScreen, 1);
        }

        public override void OnLine()
        {
            clouds = new List<Cloud>();
            speeds = new List<float>();
            for (int i = 0; i < nextScreen.maxCloud; i++)
            {
                Cloud c = new Cloud();
                clouds.Add(c);
                speeds.Add((float)Game1.rnd.NextDouble() * 1000 + 800);
            }
            nextScreen.Clouds = clouds;
            actionNum = 0;
            startCloud = false;
            cloudsOnline = false;
            cloudTime = 0;
            inProgress = false;
            twily.Reset(new Vector2(150, Cnt.ground));
            rainbow.Reset(new Vector2(600, Cnt.ground - 90));


            base.OnLine();
        }
    }
}
