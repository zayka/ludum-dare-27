﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD27_Zayka
{
    public class LevelUpScreen:Screen
    {
        //MainScreen mainscreen;
        Screen nextScreen;
        Screen NextScreen
        {
            get
            {
                if (nextScreen == null)
                {
                    nextScreen = Cnt.game.sManager.GetScreenByName("Intro");
                }
                return nextScreen;
            }
        }

        List<AbilityLevel> abilities;

        float minRSPower = 0;
        float minSSPower = 0;
        float minSTPower = 0;
        float minCPower = 0;
        //float coolness = 0;
        float score=1000;

        Effect bgEffect;
        Texture2D bgTex;
        float currentTime;
        SpriteFont font;
        Button goNext;
        int price = 50;

        public LevelUpScreen(ScreenManager manager, string name)
            : base(manager, name)
        {
         //   mainscreen = Cnt.game.sManager.GetScreenByName("MainGame") as MainScreen;

            abilities = new List<AbilityLevel>();

            AbilityLevel a;

            a = new AbilityLevel(TextureManager.Textures["Rstrike"], new Vector2(250, 200), "RainbowStrike Power: ", GetRPower, AddRPower, SubRPower);
            abilities.Add(a);

            a = new AbilityLevel(TextureManager.Textures["Speedstrut"], new Vector2(250, 350), "Speedstrut Power: ", GetSSPower, AddSSPower, SubSSPower);
            abilities.Add(a);

            a = new AbilityLevel(TextureManager.Textures["Slowtime"], new Vector2(250, 500), "Slowtime Power: ", GetSTPower, AddSTPower, SubSTPower);
            abilities.Add(a);

            a = new AbilityLevel(TextureManager.Textures["Coolness"], new Vector2(250, 650), "Coolness: ", GetCPower, AddCPower, SubCPower);
            abilities.Add(a);

            bgEffect = Cnt.game.Content.Load<Effect>("bgEffect");
            bgTex = TextureManager.Textures["BGLevelUP"];
            font = Cnt.game.Content.Load<SpriteFont>("AbilityFont");
            goNext = new Button(TextureManager.Textures["Button"], new Rectangle((int)(Game1.screenWidth*0.8f), (int)(Game1.screenHeight*0.9f), 150, 60), "continue");
            goNext.OnClick += Reset;
        }

        public override void Update(float elapsed)
        {
            currentTime += 4*elapsed;
            Screen s = Cnt.game.sManager.GetScreenByName("MainGame");
            if (InputState.IsNewKeyPressed(Keys.F2)) Cnt.game.transition.Transit(s, 0.5f);
           

            foreach (var a in abilities)
            {
                a.Update();
            }
            if (InputState.IsNewKeyPressed(Keys.F3)) { }

            goNext.Update();
            base.Update(elapsed);
        }

        public override void Draw(float elapsed)
        {
            Cnt.game.GraphicsDevice.Clear(Color.CornflowerBlue);
            SpriteBatch sb = Game1.spriteBatch;
            //bg
            bgEffect.Parameters["sinus"].SetValue((float)Math.Sin(currentTime));
            bgEffect.Parameters["cosinus"].SetValue((float)Math.Cos(currentTime));            
            sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, bgEffect);
            sb.Draw(bgTex,Vector2.Zero,Color.White);
            sb.End();
            //


            sb.Begin();
            
            DrawText("Somewhere in strange place",new Vector2(Game1.screenWidth*0.5f, 15));
            DrawText("You can spend you score points here", new Vector2(170, Game1.screenHeight*0.09f), 0.6f);
            DrawText("You score: "+score.ToString("00."), new Vector2(20, Game1.screenHeight * 0.11f), 0.6f, false);
            DrawText("Each upgrade cost 50 core points.", new Vector2(20, Game1.screenHeight * 0.13f), 0.6f, false);

            foreach (var a in abilities)
            {
                a.Draw();
            }
            goNext.Draw();
            sb.End();
            base.Draw(elapsed);
        }

        void DrawText(string text, Vector2 pos, float scale=1, bool alignCenter = true)
        {
            Vector2 textOrigin = font.MeasureString(text) * 0.5f;
            if (!alignCenter) textOrigin.X = 0;
            Game1.spriteBatch.DrawString(font, text, pos, Color.White, 0, textOrigin, scale, SpriteEffects.None, 1);
        }

        public override void OnLine()
        {
            minRSPower = Cnt.player.rainbowStikePower;
            minSSPower = Cnt.player.speedStrutPower;
            minCPower = Cnt.player.coolness;
            minSTPower = Cnt.player.slowTimePower;
            score = Cnt.player.CurrentScore;
            SongManager.Play("LevelUpTheme");
            base.OnLine();
        }

        public float GetRPower()
        {
            return Cnt.player.rainbowStikePower;
        }

        public void AddRPower(object sender, EventArgs e)
        {
            if (Cnt.player.rainbowStikePower >= 1.2f) return;
            if (score >= price)
            {
                Cnt.player.rainbowStikePower += 0.2f;
                score -= price;
            }
        }
        public void SubRPower(object sender, EventArgs e)
        {
            if (Cnt.player.rainbowStikePower - 0.2f >= minRSPower)
            {
                Cnt.player.rainbowStikePower -= 0.2f;
                score += price;
            }
        }

        public float GetSSPower()
        {
            return Cnt.player.speedStrutPower;
        }

        public void AddSSPower(object sender, EventArgs e)
        {
            if (Cnt.player.speedStrutPower >= 1.2f) return;
            if (score >= price)
            {
                Cnt.player.speedStrutPower += 0.2f;
                score -= price;
            }
        }
        public void SubSSPower(object sender, EventArgs e)
        {
            if (Cnt.player.speedStrutPower - 0.2f >= minSSPower)
            {
                Cnt.player.speedStrutPower -= 0.2f;
                score += price;
            }
        }


        public float GetSTPower()
        {
            return Cnt.player.slowTimePower;
        }

        public void AddSTPower(object sender, EventArgs e)
        {
            if (Cnt.player.slowTimePower >= 1.2f) return;
            if (score >= price)
            {
                Cnt.player.slowTimePower += 0.2f;
                score -= price;
            }
        }
        public void SubSTPower(object sender, EventArgs e)
        {
            if (Cnt.player.slowTimePower - 0.2f >= minSTPower)
            {
                Cnt.player.slowTimePower -= 0.2f;
                score += price;
            }
        }


        public float GetCPower()
        {
            return Cnt.player.coolness;
        }

        public void AddCPower(object sender, EventArgs e)
        {
            if (Cnt.player.coolness >= 1.2f) return;
            if (score >= price)
            {
                Cnt.player.coolness += 0.2f;
                score -= price;
            }
        }
        public void SubCPower(object sender, EventArgs e)
        {
            if (Cnt.player.coolness - 0.2f >= minCPower)
            {
                Cnt.player.coolness -= 0.2f;
                score += price;
            }
        }

        public void Reset(object sender, EventArgs e)
        {
            Cnt.player.SetScore(score);
            Cnt.game.transition.Transit(NextScreen, 2);
        }
    }
}
