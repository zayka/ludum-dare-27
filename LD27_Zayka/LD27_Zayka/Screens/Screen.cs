﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LD27_Zayka
{
    public class Screen
    {
        public string screenName;
        ScreenManager manager;
        public bool canPause = false;

        public Screen(ScreenManager manager, string name)
        {
            this.manager = manager;
            this.screenName = name;

        }

        public virtual void Update(float elapsed)
        {
        }

        public virtual void Draw(float elapsed)
        {
        }

        public virtual void OnLine()
        { }
    }
}
