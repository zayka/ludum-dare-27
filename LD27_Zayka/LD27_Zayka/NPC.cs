﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD27_Zayka
{
    class NPC
    {
        enum NPCstate
        {
            Walk,
            Talk,
            Stand
        }

        public Vector2 pos;
        Vector2 origin;
        public float angle;
        float maxAngle = MathHelper.PiOver4 / 3;
        float time;
        Texture2D tex;
        Vector2 target;
        NPCstate state = NPCstate.Stand;
        Vector2 speed = new Vector2(100, 100);
        IntroScreen.NPCCallBack callback;

        string text;
        float actionDelay;
        SpriteFont speechFont;

        public NPC(Vector2 pos, Texture2D tex)
        {
            this.tex = tex;
            this.pos = pos;
            origin = new Vector2(tex.Width * 0.5f, tex.Height * 0.5f);
            speechFont = Cnt.game.Content.Load<SpriteFont>("NPCSpeechFont");
            state = NPCstate.Stand;
        }


        public void Update()
        {
            time += Cnt.elapsed;
            switch (state)
            {
                case NPCstate.Walk:
                    pos += speed * Cnt.elapsed;
                    angle = maxAngle * (float)Math.Sin(time * 10 * speed.X / 50);
                    if ((target - pos).LengthSquared() < 10) { angle = 0; state = NPCstate.Stand; actionDelay = 999; callback(); }
                    break;
                case NPCstate.Talk:
                    if (time > actionDelay) { state = NPCstate.Stand; actionDelay = 999; callback(); }
                    break;
                case NPCstate.Stand:
                    if (time > actionDelay) { state = NPCstate.Stand; if (callback != null) callback(); }
                    break;
                default:
                    break;
            }
        }

        public void Draw()
        {
            SpriteBatch sb = Game1.spriteBatch;
            sb.Draw(tex, pos, null, Color.White, angle, origin, 1, SpriteEffects.None, 1);

            if (state == NPCstate.Talk)
            {
                Vector2 textOrigin = 0.5f * (speechFont.MeasureString(text));
                sb.DrawString(speechFont, text, pos - new Vector2(0, 30), Color.MidnightBlue, 0, textOrigin, 1, SpriteEffects.None, 1);
            }
            if (state == NPCstate.Stand)
            {
                //  sb.DrawString(speechFont, time.ToString("00.00"), pos - new Vector2(0, 50), Color.Green);
            }
        }

        public void WalkStart(float speed, object endPoint, IntroScreen.NPCCallBack callback)
        {
            target = (Vector2)endPoint;
            Vector2 dir = target - pos;
            dir.Normalize();
            dir *= speed;
            this.speed = dir;
            this.callback = callback;
            this.state = NPCstate.Walk;
            time = 0;
        }

        public void TalkStart(float param, object text, IntroScreen.NPCCallBack callback)
        {
            this.actionDelay = param;
            this.text = text.ToString();
            this.callback = callback;
            this.state = NPCstate.Talk;
            time = 0;
        }


        public void StandStart(float param, object x, IntroScreen.NPCCallBack callback)
        {
            this.state = NPCstate.Stand;
            this.actionDelay = param;
            this.callback = callback;
            time = 0;

        }

        public void Reset(Vector2 pos)
        {
            state = NPCstate.Stand;
            this.pos = pos;
            time = 0;
        }
    }
}
