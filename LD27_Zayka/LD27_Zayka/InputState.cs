﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace LD27_Zayka
{
    public static class InputState
    {
        static KeyboardState kstate;
        static MouseState mstate;
        static KeyboardState oldkstate;
        static MouseState oldmstate;

        public static void Update()
        {
            oldkstate = kstate;
            oldmstate = mstate;
            kstate = Keyboard.GetState();
            mstate = Mouse.GetState();
        }

        public static bool IsKeyPressed(Keys key)
        {
            return kstate.IsKeyDown(key);
        }

        public static bool IsLeftButtonClick()
        {
            return (mstate.LeftButton == ButtonState.Pressed) && (oldmstate.LeftButton != ButtonState.Pressed);
        }

        public static bool IsRightButtonClick()
        {
            return (mstate.RightButton == ButtonState.Pressed) && (oldmstate.RightButton != ButtonState.Pressed);
        }

        public static bool IsLeftButtonPress()
        {
            return mstate.LeftButton == ButtonState.Pressed;
        }

        public static bool IsLeftButtonRelease()
        {
            return (mstate.LeftButton == ButtonState.Released) && (oldmstate.LeftButton != ButtonState.Released);
        }

        public static bool IsRightButtonPress()
        {
            return mstate.RightButton == ButtonState.Pressed;
        }

        public static  bool IsNewKeyPressed(Keys key)
        {
            return (kstate.IsKeyDown(key) && !oldkstate.IsKeyDown(key));
        }

        public static bool isHover(Rectangle rect)
        {
            Vector2 currentMouse =MouseVector();
            return currentMouse == Vector2.Clamp(currentMouse, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right,rect.Bottom));
        }

        public static Vector2 MouseVector()
        {           
            return Vector2.Transform(new Vector2(mstate.X, mstate.Y), Cnt.game.Camera.InvView);
        }

        public static bool isMouseScrollUp()
        {            
            return mstate.ScrollWheelValue - oldmstate.ScrollWheelValue < 0;            
        }

        public static bool isMouseScrollDown()
        {
            return mstate.ScrollWheelValue - oldmstate.ScrollWheelValue > 0;
        }

        public static Vector2 MouseLastDist()
        {
            return new Vector2(mstate.X, mstate.Y)-new Vector2(oldmstate.X, oldmstate.Y);
        }

        public static bool IsKeyReleased(Keys key)
        {
            return (kstate.IsKeyUp(key) && !oldkstate.IsKeyUp(key));
        }

        public static bool isHoverGUI(Rectangle rect)
        {
            Vector2 currentMouse = MouseVector(true);
            return currentMouse == Vector2.Clamp(currentMouse, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
        }

        public static Vector2 MouseVector(bool GUI = false)
        {
            if (GUI) return new Vector2(mstate.X, mstate.Y);
            else
                return Vector2.Transform(new Vector2(mstate.X, mstate.Y), Cnt.game.Camera.InvView);
        }
    }
}
