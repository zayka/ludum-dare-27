﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace LD27_Zayka
{
    public class Player
    {
        AnimSprite sprite;

        public Vector2 pos;
        //Vector2 speed;
        float accelerate = 0.5f;
        public float speed;
        float maxSpeed = 10;
        float angle;
        List<BaseEmitter> emiters;

        //abilities
        public float slowTimePower = 0.0f;
        public float rainbowStikePower = 0.0f;
        public float speedStrutPower = 0.2f;
        public float coolness = 0.4f;
        public float rainbowStrikeCD = 7;
        public float speedStrutCD = 4;
        public float slowTimeCD = 5;

        public float DPS = 200;

        public bool isHaveSpeedStrut { get { return speedStrutPower > 0; } }
        public bool isHaveRainbowStrike { get { return rainbowStikePower > 0; } }
        public bool isHaveSlowTime { get { return slowTimePower > 0; } }

        //SoundEffectInstance whooshSound;

        float currentScore = 0;
        public float CurrentScore { get { return currentScore; } }

        // key config
        Keys KeyRainbowStrike = Keys.Q;
        Keys KeySpeedStrutKey = Keys.W;

        public Rectangle rect { get { return new Rectangle((int)(pos.X - sprite.Origin.X), (int)(pos.Y - sprite.Origin.Y), sprite.Width, sprite.Height); } }

        public Player()
        {


            sprite = new AnimSprite(TextureManager.Textures["Player"], 30, 30, 1, 1);
            pos = new Vector2(500, Cnt.ground);
            emiters = new List<BaseEmitter>();
            DotEmitter e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.Purple;
            emiters.Add(e);

            e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.DeepSkyBlue;
            emiters.Add(e);

            e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.Green;
            emiters.Add(e);

            e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.Gold;
            emiters.Add(e);

            e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.DarkOrange;
            emiters.Add(e);

            e = new DotEmitter("p4", Cnt.pEngine, false);
            SetUpEmitter(e);
            e.color = Color.Crimson;
            emiters.Add(e);

        }



        public void Update()
        {
            float elapsed = Cnt.elapsed;

            sprite.Update(elapsed);

            PositioningEmitters();
            Vector2 mouse = InputState.MouseVector();
            Vector2 dir = mouse - pos;
            angle = (float)Math.Atan2(dir.Y, dir.X);
            HandleInput();

            speed = MathHelper.Clamp(speed, 0, maxSpeed);
            if (dir.LengthSquared() > 100)
            {
                dir.Normalize();
                pos += dir * speed;
                pos = Vector2.Clamp(pos, Vector2.Zero, new Vector2(Game1.screenWidth, Game1.screenHeight));
                if ((dir * speed).LengthSquared() > 5)
                {
                    foreach (var e in emiters) { e.UpdateEmitter(); }
                }
            }
            //  if (whooshSound.State == SoundState.Stopped && speed > 0) whooshSound.Play();
        }

        private void HandleInput()
        {
            if (InputState.IsLeftButtonPress())
            {
                speed += accelerate;
            }
            else speed -= 2 * accelerate;

            if (InputState.IsNewKeyPressed(KeyRainbowStrike)) { }
            if (InputState.IsNewKeyPressed(KeySpeedStrutKey)) { }
        }


        public void Draw()
        {
            SpriteBatch sb = Game1.spriteBatch;
            sprite.Draw(pos, Color.White, angle);
        }

        private void SetUpEmitter(BaseEmitter e)
        {
            e.minSize = 0.7f;
            e.maxSize = 1.1f;
            e.Pos = e.prevPos = pos;
            e.Pps = 100;
            e.maxSpeed = 0;
            e.minSpeed = 0;
            e.tMaxAlpha = 0.0f;
        }

        private void PositioningEmitters()
        {
            float a = 0.1f;
            float x = 0.16f;//(1-2*a)/(numEmmiters-1);

            Vector2 back = new Vector2(-sprite.Width * 0.5f * 0.2f, -sprite.Height * 0.5f + sprite.Height * a);

            for (int i = 0; i < 6; i++)
            {
                emiters[i].position = pos + back;
                back.Y += x * sprite.Height;
            }
        }


        public void AddScore(float rawScore)
        {
            currentScore += rawScore * 0.035f;
        }
        public void SetScore(float score)
        {
            currentScore = score;
        }

    }
}
