﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LD27_Zayka
{
    public class Transition
    {
        public enum TType
        {
            Normal,
            Warp,
        }

        public float transitionTime = 5.0f;
        public float currentTime = 0;
        // public delegate void DrawFunc(GameTime gametime);
        // DrawFunc prevDraw, nextDraw;
        //Game1.GameState nextState;
        Screen nextScreen;
        Screen currentScreen;
        public RenderTarget2D target;
        public RenderTarget2D warpTarget;
        public bool isInTransition { get { return currentTime < transitionTime; } }
        TType transitionType;
        Effect warpEffect;

        public Transition()
        {
            target = new RenderTarget2D(Cnt.game.GraphicsDevice, Game1.screenWidth, Game1.screenHeight);
            warpTarget = new RenderTarget2D(Cnt.game.GraphicsDevice, Game1.screenWidth, Game1.screenHeight);
            transitionTime = 0;
            currentTime = 0;
            warpEffect = Cnt.game.Content.Load<Effect>("transitionWarp");
        }

        public void Update()
        {
            currentTime += Cnt.elapsed;
            if (currentTime > transitionTime * 0.5f) { currentScreen = nextScreen; }
            if (InputState.IsNewKeyPressed(Keys.Space) || InputState.IsLeftButtonClick()) currentTime = transitionTime;

            if (currentTime >= transitionTime) Cnt.game.sManager.SetActiveScreen(nextScreen);
        }

        public void Transit(Screen nextScreen, float time, Transition.TType type = TType.Normal)
        {
            SongManager.Stop();
            transitionType = type;
            if (time == 0)
            {
                Cnt.game.sManager.SetActiveScreen(nextScreen);
                nextScreen.OnLine();
                return;
            }
            this.nextScreen = nextScreen;
            this.currentScreen = Cnt.game.sManager.GetActiveScreen();
            currentTime = 0;
            transitionTime = time;
            nextScreen.OnLine();
        }

        public void Draw(GameTime gt)
        {
            if (transitionTime == 0) return;
            float t = currentTime / transitionTime;

            Color color = Color.White;

            Cnt.game.GraphicsDevice.SetRenderTarget(target);

            currentScreen.Draw((float)gt.ElapsedGameTime.Seconds);

            if (t < 0.5)
            {
                color = Color.Lerp(Color.White, Color.Black, t / 0.5f);
            }
            else
            {
                color = Color.Lerp(Color.Black, Color.White, (t - 0.5f) / 0.5f);
            }


            if (transitionType == TType.Warp)
            {
                if (t < 0.5f)
                { DrawWarp(t, false); return; }
                else
                { DrawWarp(t, true); return; }
            }

            Cnt.game.GraphicsDevice.SetRenderTarget(null);
            Game1.spriteBatch.Begin();
            Game1.spriteBatch.Draw(target, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), null, color);
            Game1.spriteBatch.End();

        }

        private void DrawWarp(float t, bool warpOut)
        {
            //Cnt.game.GraphicsDevice.Textures[0] = target;

            Cnt.game.GraphicsDevice.SetRenderTarget(warpTarget);
            warpEffect.Parameters["time"].SetValue(t * 2);
            warpEffect.Parameters["warpOut"].SetValue(warpOut);
            Game1.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, warpEffect);
            Game1.spriteBatch.Draw(target, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), null, Color.White);
            Game1.spriteBatch.End();

            Cnt.game.GraphicsDevice.SetRenderTarget(null);
            Game1.spriteBatch.Begin();
            Game1.spriteBatch.Draw(warpTarget, new Rectangle(0, 0, Game1.screenWidth, Game1.screenHeight), null, Color.White);
            Game1.spriteBatch.End();
        }
    }
}
