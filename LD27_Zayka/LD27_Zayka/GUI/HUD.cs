﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LD27_Zayka
{
    public class HUD
    {
        MainScreen mainscreen;
        Texture2D RStex, SStex, STtex, SquareTex;
        SpriteFont timerFont, abilityFont;
        Player player { get { return Cnt.player; } }

        public HUD(MainScreen screen)
        {
            RStex = TextureManager.Textures["Rstrike"];
            SStex = TextureManager.Textures["Speedstrut"];
            STtex = TextureManager.Textures["Slowtime"];
            SquareTex = TextureManager.Textures["Square"];
            timerFont = Cnt.game.Content.Load<SpriteFont>("timerFont");
            abilityFont = Cnt.game.Content.Load<SpriteFont>("AbilityFont");
            mainscreen = screen;
        }

        public void Update()
        {

        }

        public void Draw()
        {
            SpriteBatch spriteBatch = Game1.spriteBatch;
            Color c1 = mainscreen.isSlowTimeActive ? Color.Red : Color.HotPink;
            Vector2 origin = timerFont.MeasureString("TIME: " + mainscreen.levelTime.ToString("0.00")) * 0.5f;
            spriteBatch.DrawString(timerFont, "TIME: " + mainscreen.levelTime.ToString("0.00"), new Vector2(Game1.screenWidth * 0.5f - 20, Game1.screenHeight - 50), c1, 0, origin, 1, SpriteEffects.None, 1);

            int width = RStex.Width / 2;
            int height = RStex.Height / 2;
            int posY = (int)(Game1.screenHeight * 0.9f);
            int posX = (int)(Game1.screenWidth * 0.02f);

            #region abilities
            Color shadowColor = new Color(0, 0, 0, 0.7f);
            if (player.isHaveRainbowStrike)
            {
                //rs
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY, width, height), null, new Color(0, 0, 0, 0.5f), 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(RStex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(SquareTex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.DrawString(abilityFont, "Q", new Vector2(posX + width * 0.25f, posY - height * 0.5f), Color.White);

                int cY = (int)MathHelper.Lerp(height, 0, mainscreen.RBTime / player.rainbowStrikeCD);
                cY = (int)MathHelper.Clamp(cY, 0, height);
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY - cY + height, width, cY), null, shadowColor, 0, Vector2.Zero, SpriteEffects.None, 1);

            }

            posX += width + 15;
            if (player.isHaveSpeedStrut)
            {
                //ss
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY, width, height), null, new Color(0, 0, 0, 0.5f), 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(SStex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(SquareTex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.DrawString(abilityFont, "W", new Vector2(posX + width * 0.25f, posY - height * 0.5f), Color.White);

                int cY = (int)MathHelper.Lerp(height, 0, mainscreen.SSTime / player.slowTimeCD);
                cY = (int)MathHelper.Clamp(cY, 0, height);
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY - cY + height, width, cY), null, shadowColor, 0, Vector2.Zero, SpriteEffects.None, 1);
            }

            posX += width + 15;
            if (player.isHaveSlowTime)
            {
                //st
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY, width, height), null, new Color(0, 0, 0, 0.5f), 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(STtex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.Draw(SquareTex, new Rectangle(posX, posY, width, height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
                spriteBatch.DrawString(abilityFont, "E", new Vector2(posX + width * 0.25f, posY - height * 0.5f), Color.White);

                int cY = (int)MathHelper.Lerp(height, 0, mainscreen.STTime / player.slowTimeCD);
                cY = (int)MathHelper.Clamp(cY, 0, height);
                spriteBatch.Draw(TextureManager.Textures["Dot"], new Rectangle(posX, posY - cY + height, width, cY), null, shadowColor, 0, Vector2.Zero, SpriteEffects.None, 1);
            }
            #endregion


            origin = abilityFont.MeasureString("Score: " + player.CurrentScore.ToString("00.0")) * 0.5f;
            spriteBatch.DrawString(abilityFont, "Score: " + player.CurrentScore.ToString("00.0"), new Vector2(Game1.screenWidth * 0.9f, Game1.screenHeight - 50), Color.Yellow, 0, origin, 1, SpriteEffects.None, 1);


        }
    }
}
