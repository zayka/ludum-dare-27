﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD27_Zayka
{   
    class Button
    {
        Vector2 position;
        public string text;
        Texture2D texture;
        Vector2 origin;
        SpriteFont mainFont;

        Rectangle rect;
        bool isHover { get { return InputState.isHoverGUI(rect); } }
        public EventHandler OnClick;
        


        public Button(Texture2D texture, Vector2 position, string text)
        {
            this.texture = texture;
            this.position = position;
            this.text = text;
            origin = new Vector2(texture.Width / 2, texture.Height / 2);
            rect = new Rectangle((int)(position.X-origin.X), (int)(position.Y-origin.Y), texture.Width, texture.Height);
            mainFont = Cnt.game.Content.Load<SpriteFont>("AbilityFont");
        }

        public Button(Texture2D texture, Rectangle rect, string text)
        {
            this.texture = texture;
            this.position = new Vector2(rect.X+rect.Width/2,rect.Y+rect.Height/2);
            this.text = text;           
            this.rect = rect;
            origin = new Vector2(rect.Width / 2, rect.Height / 2);
            mainFont = Cnt.game.Content.Load<SpriteFont>("AbilityFont");
        }

        public void Update()
        {
            if (isHover && InputState.IsLeftButtonClick()&&OnClick!=null) OnClick(this, EventArgs.Empty);
        }

        public void Draw()
        {
            SpriteBatch sb = Game1.spriteBatch;
            Vector2 m = 0.5f * mainFont.MeasureString(text);
            if (isHover)
            {sb.Draw(texture, rect, null, Color.Gray, 0, Vector2.Zero,  SpriteEffects.None, 1); }
            else
                sb.Draw(texture, rect, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
            sb.DrawString(mainFont, text, position, Color.White, 0, m, 1, SpriteEffects.None, 1);
        }

    }
}
