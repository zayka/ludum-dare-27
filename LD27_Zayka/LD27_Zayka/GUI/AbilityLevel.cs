﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace LD27_Zayka
{
    class AbilityLevel
    {
        public delegate float GetParameter();
        GetParameter getparameter;
        static Texture2D levelEmpty;
        static Texture2D levelFill;
        static Texture2D LevelEmpty
        {
            get
            {
                if (levelEmpty == null)
                    levelEmpty = TextureManager.Textures["LevelEmpty"];
                return levelEmpty;
            }
        }
        static Texture2D LevelFill
        {
            get
            {
                if (levelFill == null)
                    levelFill = TextureManager.Textures["LevelFill"];
                return levelFill;
            }
        }


        List<Button> buttons;
        Texture2D background;
        Texture2D pictogram;
        Vector2 position;
        Vector2 origin;
        SpriteFont font;
        string abilityText;
        Vector2 pictOrigin;

        SoundEffectInstance blipSound;

        public AbilityLevel(Texture2D pictogram, Vector2 position, string text, GetParameter getparameter, EventHandler addParameter, EventHandler subParameter)
        {
            this.getparameter = getparameter;
            this.pictogram = pictogram;
            this.background = TextureManager.Textures["abilityBG"];
            this.position = position;
            this.abilityText = text;
            origin = new Vector2(background.Width / 2, background.Height / 2);
            pictOrigin = new Vector2(pictogram.Width / 2, pictogram.Height / 2);
            buttons = new List<Button>();
            Texture2D buttonP = TextureManager.Textures["ButtonPlus"];
            Texture2D buttonM = TextureManager.Textures["ButtonMinus"];
            Button b = new Button(buttonM, new Vector2(position.X + origin.X + buttonM.Width * 0.5f, position.Y), "");
            b.OnClick += subParameter;
            b.OnClick += BlipSound;
            buttons.Add(b);

            b = new Button(buttonP, new Vector2(position.X + origin.X + buttonM.Width + buttonP.Width * 0.5f, position.Y), "");
            b.OnClick += addParameter;
            b.OnClick += BlipSound;
            buttons.Add(b);
            font = Cnt.game.Content.Load<SpriteFont>("AbilityFont");
            SoundEffect blipSoundEffect = Cnt.game.Content.Load<SoundEffect>("Sound\\blip");
            blipSound = blipSoundEffect.CreateInstance();
        }
          

        public void Update()
        {
            foreach (var b in buttons)
            {
                b.Update();
            }

        }

        public void Draw()
        {
            float param = getparameter() * 5;// /0.2  0-0.2-0.4-0.6-0.8-1.0-1.2  -> 0-1-2-3-4-5-6
            int numFill = (int)param;
            int numEmpty = 6 - numFill;

            SpriteBatch sb = Game1.spriteBatch;
            Vector2 m = font.MeasureString(abilityText);
            sb.Draw(background, position, null, Color.White, 0, origin, 1, SpriteEffects.None, 1);
            sb.Draw(pictogram, new Vector2(position.X - origin.X + pictOrigin.X + 3, position.Y), null, Color.White, 0, pictOrigin, 1, SpriteEffects.None, 1);
            sb.DrawString(font, abilityText + (100*getparameter()).ToString("0.")+"%", position - new Vector2(origin.X, origin.Y + m.Y), Color.White);

            Vector2 levelOrigin = new Vector2(LevelEmpty.Width * 0.5f, LevelEmpty.Height * 0.5f);
            Vector2 start = new Vector2(position.X - origin.X + pictOrigin.X * 2+6+levelOrigin.X+6, position.Y);
            
            for (int i = 0; i < numFill; i++)
            {
                sb.Draw(LevelFill, start, null, Color.White, 0, levelOrigin, 1, SpriteEffects.None, 1);
                start.X += levelOrigin.X * 2;
            }
            
            for (int i = 0; i < numEmpty; i++)
            {
                sb.Draw(LevelEmpty, start, null, Color.White, 0, levelOrigin, 1, SpriteEffects.None, 1);
                start.X += levelOrigin.X * 2;
            }
            

            foreach (var b in buttons)
            {
                b.Draw();
            }
        }

        void BlipSound(object sender, EventArgs e)
        {
            blipSound.Play();
        }

    }
}
