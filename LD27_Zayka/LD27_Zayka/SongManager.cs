﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;

namespace LD27_Zayka
{
    public static class SongManager
    {

        public static Dictionary<string, Song> Songs = new Dictionary<string, Song>();
        private static bool init = false;

        public static void Init()
        {
            Songs.Add("MainTheme",Cnt.game.Content.Load<Song>("Sound\\Song\\01_Main"));
            Songs.Add("LevelUpTheme", Cnt.game.Content.Load<Song>("Sound\\Song\\02_LevelUP"));

            init = true;
        }


        public static void Play(string name)
        {
            if (Cnt.noMusic) return;
            if (!init) Init();
            if (MediaPlayer.State != MediaState.Playing)
                MediaPlayer.Play(Songs[name]);            
        }
        public static void Stop()
        {
            MediaPlayer.Stop();
        }

        
    }
}
